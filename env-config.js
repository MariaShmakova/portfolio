const prod = process.env.NODE_ENV === "production";

module.exports = {
  "process.env.BASE_URL": prod
    ? "https://shmakova-maria.herokuapp.com"
    : "http://localhost:3000",
  "process.env.NAMESPACE": "https://shmakova-maria.herokuapp.com",
  "process.env.AUTH0_CLIENT_ID": "UK5jYzRLQjkaa8n0XbfGfDNO8bCfODZD",
  "process.env.AUTH0_DOMAIN": "dev-ar97jw4k.eu.auth0.com",
  "process.env.AUTH0_JWKS_URL":
    "https://dev-ar97jw4k.eu.auth0.com/.well-known/jwks.json"
};
