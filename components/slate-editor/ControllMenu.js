import React from "react";
import { Button } from "reactstrap";

const ControllMenu = props => {
  const { onSave, isSaving } = props;

  return (
    <div className="controll-menu">
      <h1 className="title">Write your story...</h1>
      <div className="status-box">{isSaving ? "Saving..." : "Saved"}</div>
      <Button disabled={isSaving} color="success" onClick={onSave}>
        Save
      </Button>
    </div>
  );
};

export default ControllMenu;
