import React from "react";
import ReactDOM from "react-dom";
import { MarkButton, BlockButton } from "./renderers";
import { Menu } from "./components";

const HoverMenu = React.forwardRef(({ editor }, ref) => {
  const root = window.document.getElementById("__next");
  return ReactDOM.createPortal(
    <Menu ref={ref} className="editor-hovering-menu">
      <MarkButton editor={editor} type="bold" icon="fas fa-bold" />
      <MarkButton editor={editor} type="italic" icon="fas fa-italic" />
      <MarkButton editor={editor} type="underlined" icon="fas fa-underline" />
      <MarkButton editor={editor} type="code" icon="fas fa-code" />
      <BlockButton editor={editor} type="heading-one" icon="fas fa-heading" />
      <BlockButton
        editor={editor}
        type="heading-two"
        icon="fas fa-heading fa-sm"
      />
      <BlockButton
        editor={editor}
        type="block-quote"
        icon="fas fa-quote-right"
      />
      <BlockButton editor={editor} type="numbered-list" icon="fas fa-list-ol" />
      <BlockButton editor={editor} type="bulleted-list" icon="fas fa-list-ul" />
    </Menu>,
    root
  );
});

export default HoverMenu;
