import { Value } from "slate";

const initialValues = Value.fromJSON({
  object: "value",
  document: {
    object: "document",
    nodes: [
      {
        object: "block",
        type: "paragraph",
        nodes: []
      }
    ]
  }
});

export default initialValues;
