import React, { Component } from "react";
import { Editor } from "slate-react";

import HoverMenu from "./HoverMenu";
import initialValue from "./initialValues";
import ControllMenu from "./ControllMenu";
import Html from "slate-html-serializer";
import { Value } from "slate";
import { rules } from "./rules";

const html = new Html({ rules });

class SlateEditor extends Component {
  state = {
    value: Value.create(),
    isLoaded: false
  };

  menuRef = React.createRef();

  componentDidMount = () => {
    const initValueFromProps = this.props.initialValue;
    const value = initValueFromProps
      ? Value.fromJSON(html.deserialize(initValueFromProps))
      : Value.fromJSON(initialValue);
    this.setState({ isLoaded: true, value });
    this.updateMenu();
  };

  componentDidUpdate = () => this.updateMenu();

  onChange = ({ value }) => this.setState({ value });

  onKeyDown = (event, change, next) => {
    const { isSaving } = this.props;

    if (!isSaving && event.which == 83 && (event.ctrlKey || event.metaKey)) {
      event.preventDefault();
      this.onSave();
    }

    next();
  };

  /**
   * Update the menu's absolute position.
   */

  updateMenu = () => {
    const menu = this.menuRef.current;
    if (!menu) return;

    const { value } = this.state;
    const { fragment, selection } = value;

    if (selection.isBlurred || selection.isCollapsed || fragment.text === "") {
      menu.removeAttribute("style");
      return;
    }

    const native = window.getSelection();
    const range = native.getRangeAt(0);
    const rect = range.getBoundingClientRect();
    menu.style.opacity = 1;
    menu.style.top = `${rect.top + window.pageYOffset - menu.offsetHeight}px`;

    menu.style.left = `${rect.left +
      window.pageXOffset -
      menu.offsetWidth / 2 +
      rect.width / 2}px`;
  };

  /**
   * Render the editor.
   */

  renderEditor = (props, editor, next) => {
    const children = next();
    return (
      <React.Fragment>
        <ControllMenu onSave={this.onSave} isSaving={props.isSaving} />
        {children}
        <HoverMenu ref={this.menuRef} editor={editor} />
        <style jsx>
          {`
            @import url("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css");
          `}
        </style>
      </React.Fragment>
    );
  };

  /**
   * Render a Slate mark.
   */

  renderMark = (props, editor, next) => {
    const { children, mark, attributes } = props;

    switch (mark.type) {
      case "bold":
        return <strong {...attributes}>{children}</strong>;
      case "code":
        return <code {...attributes}>{children}</code>;
      case "italic":
        return <em {...attributes}>{children}</em>;
      case "underlined":
        return <u {...attributes}>{children}</u>;
      default:
        return next();
    }
  };

  renderBlock = (props, editor, next) => {
    const { attributes, children, node } = props;

    switch (node.type) {
      case "paragraph":
        return <p {...attributes}>{children}</p>;
      case "block-quote":
        return <blockquote {...attributes}>{children}</blockquote>;
      case "bulleted-list":
        return <ul {...attributes}>{children}</ul>;
      case "heading-one":
        return <h1 {...attributes}>{children}</h1>;
      case "heading-two":
        return <h2 {...attributes}>{children}</h2>;
      case "list-item":
        return <li {...attributes}>{children}</li>;
      case "numbered-list":
        return <ol {...attributes}>{children}</ol>;
      default:
        return next();
    }
  };

  getTitle = () => {
    const { value } = this.state;

    const firstBlock = value.document.getBlocks().get(0);
    const secondBlock = value.document.getBlocks().get(1);
    const title = firstBlock && firstBlock.text ? firstBlock.text : "No title";
    const subtitle =
      secondBlock && secondBlock.text ? secondBlock.text : "No subtitle";

    return { title, subtitle };
  };

  onSave = () => {
    const { value } = this.state;
    const { onSave, isSaving } = this.props;
    const heading = this.getTitle();
    const story = html.serialize(value);

    if (!isSaving) onSave(story, heading);
  };

  render() {
    const { isLoaded } = this.state;

    if (!isLoaded) return null;

    return (
      <Editor
        {...this.props}
        placeholder="Enter some text..."
        value={this.state.value}
        onChange={this.onChange}
        onKeyDown={this.onKeyDown}
        renderEditor={this.renderEditor}
        renderMark={this.renderMark}
        renderBlock={this.renderBlock}
      />
    );
  }
}

export default SlateEditor;
