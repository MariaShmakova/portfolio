import React from "react";
import { Button, Icon } from "../components";

const DEFAULT_NODE = "paragraph";

export const MarkButton = ({ editor, type, icon }) => {
  const { value } = editor;
  const isActive = value.activeMarks.some(mark => mark.type === type);
  return (
    <Button
      reversed
      active={isActive}
      onMouseDown={event => {
        event.preventDefault();
        editor.toggleMark(type);
      }}
    >
      <Icon className={icon} />
    </Button>
  );
};

const hasBlock = (type, editor) => {
  const { value } = editor;
  return value.blocks.some(node => node.type === type);
};

const onClickBlock = (event, type, editor) => {
  event.preventDefault();

  const { value } = editor;
  const { document } = value;

  // Handle everything but list buttons.
  if (type !== "bulleted-list" && type !== "numbered-list") {
    const isActive = hasBlock(type, editor);
    const isList = hasBlock("list-item", editor);

    if (isList) {
      editor
        .setBlocks(isActive ? DEFAULT_NODE : type)
        .unwrapBlock("bulleted-list")
        .unwrapBlock("numbered-list");
    } else {
      editor.setBlocks(isActive ? DEFAULT_NODE : type);
    }
  } else {
    // Handle the extra wrapping required for list buttons.
    const isList = hasBlock("list-item", editor);
    const isType = value.blocks.some(block => {
      return !!document.getClosest(block.key, parent => parent.type === type);
    });

    if (isList && isType) {
      editor
        .setBlocks(DEFAULT_NODE)
        .unwrapBlock("bulleted-list")
        .unwrapBlock("numbered-list");
    } else if (isList) {
      editor
        .unwrapBlock(
          type === "bulleted-list" ? "numbered-list" : "bulleted-list"
        )
        .wrapBlock(type);
    } else {
      editor.setBlocks("list-item").wrapBlock(type);
    }
  }
};

export const BlockButton = ({ type, icon, editor }) => {
  let isActive = hasBlock(type, editor);

  if (["numbered-list", "bulleted-list"].includes(type)) {
    const {
      value: { document, blocks }
    } = editor;

    if (blocks.size > 0) {
      const parent = document.getParent(blocks.first().key);
      isActive =
        hasBlock("list-item", editor) && parent && parent.type === type;
    }
  }

  return (
    <Button
      reversed
      active={isActive}
      onMouseDown={event => onClickBlock(event, type, editor)}
    >
      <Icon className={icon} />
    </Button>
  );
};


