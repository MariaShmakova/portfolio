import React from "react";
import BaseLayout from "../layout/BaseLayout";
import BasePage from "../BasePage";

export default role => Component =>
  class withAuth extends React.Component {
    static async getInitialProps(ctx) {
      const pageProps =
        Component.getInitialProps && (await Component.getInitialProps(ctx));
      return { ...pageProps };
    }

    render() {
      const { isAuthenticated, user } = this.props.auth;
      
      const userRole = user && user[`${process.env.NAMESPACE}/role`];
      let isAuthorized = false;

      if (role) {
        if (userRole && userRole === role) isAuthorized = true;
      } else {
        isAuthorized = true;
      }

      if (!isAuthenticated) {
        return (
          <BaseLayout {...this.props.auth}>
            <BasePage>
              <h1>
                You are not authenticated. Please login to access this page.
              </h1>
            </BasePage>
          </BaseLayout>
        );
      } else if (!isAuthorized) {
        return (
          <BaseLayout {...this.props.auth}>
            <BasePage>
              <h1>
                You are not authorized. You don't have permission to visit this
                page.
              </h1>
            </BasePage>
          </BaseLayout>
        );
      }

      return <Component {...this.props} />;
    }
  };
