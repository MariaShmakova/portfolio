import React from "react";
import { Formik, Form as FormEl, Field } from "formik";
import Joi from "joi-browser";
import { Button } from "reactstrap";

import PortInput from "./../form/PortInput";
import PortDate from "./../form/PortDate";
import Form from "../form/Form";

class PortfolioCreateForm extends Form {
  state = {
    errors: {}
  };

  schema = {
    title: Joi.string()
      .required()
      .label("Title"),
    company: Joi.string()
      .required()
      .label("Company"),
    location: Joi.string()
      .required()
      .label("Location"),
    position: Joi.string()
      .required()
      .label("Position"),
    description: Joi.string()
      .required()
      .label("Description"),
    startDate: Joi.date()
      .required()
      .label("Start Date"),
    endDate: Joi.date()
      .allow(null)
      .min(Joi.ref("startDate"))
      .label("End Date"),
    userId: Joi.string(),
    _id: Joi.string()
  };

  render() {
    const { initialValues, onSubmit } = this.props;
    return (
      <div>
        <Formik
          initialValues={initialValues}
          validate={this.validate}
          onSubmit={onSubmit}
        >
          {({ isSubmitting }) => (
            <FormEl>
              <Field
                type="text"
                name="title"
                label="Title"
                component={PortInput}
              />
              <Field
                type="text"
                name="company"
                label="Company"
                component={PortInput}
              />
              <Field
                type="text"
                name="location"
                label="Location"
                component={PortInput}
              />
              <Field
                type="text"
                name="position"
                label="Position"
                component={PortInput}
              />
              <Field
                type="textarea"
                name="description"
                label="Description"
                component={PortInput}
              />
              <Field name="startDate" label="Start Date" component={PortDate} />
              <Field
                name="endDate"
                label="End Date"
                canBeDisabled={true}
                component={PortDate}
              />

              <Button
                color="success"
                size="lg"
                type="submit"
                disabled={isSubmitting}
              >
                Submit
              </Button>
            </FormEl>
          )}
        </Formik>
      </div>
    );
  }
}

export default PortfolioCreateForm;
