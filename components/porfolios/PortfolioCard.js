import React, { Component } from "react";
import { Card, CardHeader, CardBody, CardTitle, CardText } from "reactstrap";
import PortfolioCardDetail from "./PortfolioCardDetail";
class PortfolioCard extends Component {
  state = {
    isOpen: false
  };

  handleToggle = () => {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  };

  render() {
    const { portfolio, children } = this.props;
    const { isOpen } = this.state;

    return (
      <React.Fragment>
        <PortfolioCardDetail
          toggle={this.handleToggle}
          isOpen={isOpen}
          portfolio={portfolio}
        />
        <Card className="base-card" onClick={this.handleToggle}>
          <CardHeader className="base-card-header">
            {portfolio.position}
          </CardHeader>
          <CardBody>
            <p className="base-card-city">{portfolio.location}</p>
            <CardTitle className="base-card-title">
              {portfolio.company}
            </CardTitle>
            <div
              className="base-card-text"
              dangerouslySetInnerHTML={{
                __html: portfolio.title
              }}
            />
            <div className="readMore">{children}</div>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default PortfolioCard;
