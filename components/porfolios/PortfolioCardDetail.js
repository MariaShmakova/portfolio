import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import moment from "moment";

class PortfolioCardDetail extends React.Component {
  render() {
    const { isOpen, toggle, portfolio } = this.props;
    return (
      <div>
        <Modal isOpen={isOpen} toggle={toggle}>
          <ModalHeader toggle={toggle}>{portfolio.title}</ModalHeader>
          <ModalBody>
            <div>
              <p>
                <b>Description: </b>
                <div
                dangerouslySetInnerHTML={{
                  __html: portfolio.description
                }}
              />
              </p>
              <p>
                <b>Company: </b>
                {portfolio.company}
              </p>
              <p>
                <b>Location: </b>
                {portfolio.location}
              </p>
              <p>
                <b>Position: </b>
                {portfolio.position}
              </p>
              <p>
                <b>Start date: </b>
                {moment(portfolio.startDate).format("MMMM YYYY")}
              </p>
              <p>
                <b>End date: </b>
                {portfolio.endDate
                  ? moment(portfolio.endDate).format("MMMM YYYY")
                  : "Still working here"}
              </p>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default PortfolioCardDetail;
