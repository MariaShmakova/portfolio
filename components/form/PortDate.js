import React from "react";
import DatePicker from "react-datepicker";
import { FormGroup, Label, Button } from "reactstrap";

import "react-datepicker/dist/react-datepicker.css";

// CSS Modules, react-datepicker-cssmodules.css
// import 'react-datepicker/dist/react-datepicker-cssmodules.css';

class PortDate extends React.Component {
  

  constructor(props) {
    super(props);
    
    const isHidden = props.field.value === null ? true : false;
    this.state = {
      isHidden
    };
  }

  handleChange = date => {
    this.setFieldValueAndTouched(date, true);
  };

  setFieldValueAndTouched = (value, touched) => {
    const {
      field: { name },
      form: { setFieldValue, setFieldTouched }
    } = this.props;

    setFieldValue(name, value, true);
    setFieldTouched(name, touched, true);
  };

  toggleDate = date => {
    this.setState({ isHidden: !this.state.isHidden });
    this.setFieldValueAndTouched(date, true);
  };

  renderDisabledButton = date => {
    const { isHidden } = this.state;

    if (isHidden) {
      return (
        <React.Fragment>
          <p>Still working here</p>
          <Button onClick={() => this.toggleDate(date)}>Set End Date</Button>
        </React.Fragment>
      );
    }

    return (
      <Button onClick={() => this.toggleDate(null)}>Still work here</Button>
    );
  };

  render() {
    const {
      label,
      form: { touched, errors },
      field: { value, name },
      canBeDisabled
    } = this.props;
    const { isHidden } = this.state;

    return (
      <FormGroup>
        <Label>{label}</Label>
        <div className="input-group">
          {!isHidden && (
            <DatePicker
              selected={value}
              onChange={this.handleChange}
              peekNextMonth
              showMonthDropdown
              showYearDropdown
              dateFormat="dd.MM.yyyy"
              maxDate={new Date()}
              dropdownMode="select"
            />
          )}
        </div>
        {canBeDisabled && this.renderDisabledButton(value)}
        {touched[name] && errors[name] && (
          <div className="error">{errors[name]}</div>
        )}
      </FormGroup>
    );
  }
}

export default PortDate;
