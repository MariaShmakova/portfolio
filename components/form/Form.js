import React, { Component } from "react";
import Joi from "joi-browser";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      errors: {}
    };
  }

  validate = values => {
  
    const options = { abortEarly: false };
    const { error } = Joi.validate(values, this.schema, options);
    if (!error) return {};

    let errors = {};

    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }

    return errors;
  };
  
}

export default Form;
