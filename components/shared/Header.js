import React from "react";
import ActiveLink from "../ActiveLink";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap";
import auth0 from "../../services/auth0";

const BsNavLink = props => {
  const { title, route } = props;
  const className = props.className || "";
  return (
    <ActiveLink activeClassName="active" route={route}>
      <a className={`nav-link port-navbar-link ${className}`}>{title}</a>
    </ActiveLink>
  );
};

const Login = () => {
  return (
    <span onClick={auth0.login} className="nav-link port-navbar-link clickable">
      Login
    </span>
  );
};

const Logout = () => {
  return (
    <span
      onClick={auth0.logout}
      className="nav-link port-navbar-link clickable"
    >
      Logout
    </span>
  );
};

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      dropdownOpen: false
    };
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  dropdownToggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };

  renderBlogMenu = () => {
    const { isSiteOwner } = this.props;

    if (isSiteOwner) {
      return (
        <ButtonDropdown
          className="port-navbar-link"
          isOpen={this.state.dropdownOpen}
          toggle={this.dropdownToggle}
        >
          <DropdownToggle className="port-dropdown-toggle" nav caret>
            Blog
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>
              <BsNavLink
                className="port-dropdown-item"
                route="/blogs"
                title="Блог"
              />
            </DropdownItem>
            <DropdownItem>
              <BsNavLink
                className="port-dropdown-item"
                route="/blogs/create"
                title="Создать пост"
              />
            </DropdownItem>
            <DropdownItem>
              <BsNavLink
                className="port-dropdown-item"
                route="/blogs/dashboard"
                title="Панель управления"
              />
            </DropdownItem>
          </DropdownMenu>
        </ButtonDropdown>
      );
    }

    return <BsNavLink route="/blogs" title="Blog" />;
  };

  render() {
    const { isAuthenticated, className } = this.props;
    const { isOpen } = this.state;
    const menuOpenClass = isOpen ? "menu-open" : "menu-close";

    return (
      <div>
        <Navbar
          className={`port-navbar port-nav-base absolute ${className} ${menuOpenClass}`}
          color="transparent"
          dark
          expand="md"
        >
          <NavbarBrand className="port-navbar-brand" href="/">
            Maria Shmakova
          </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem className="port-navbar-item">
                <BsNavLink route="/" title="Home" />
              </NavItem>
              {/* <NavItem className="port-navbar-item">
                <BsNavLink route="/about" title="About" />
              </NavItem> */}
              <NavItem className="port-navbar-item">
                <BsNavLink route="/portfolios" title="History Jobs" />
              </NavItem>
              <NavItem className="port-navbar-item">
                <BsNavLink route="/projects" title="Projects" />
              </NavItem>
              <NavItem className="port-dropdown-menu">
                {this.renderBlogMenu()}
              </NavItem>
              {/* <NavItem className="port-navbar-item">
                <BsNavLink route="/cv" title="Cv" />
              </NavItem> */}
              {!isAuthenticated && (
                <NavItem className="port-navbar-item" style={{display: 'none'}}>
                  <Login />
                </NavItem>
              )}
              {isAuthenticated && (
                <NavItem className="port-navbar-item" style={{display: 'none'}}>
                  <Logout />
                </NavItem>
              )}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
