import React from "react";
import Header from "../shared/Header";
import Head from "next/head";

const BaseLayout = props => {
  const {
    children,
    className,
    isAuthenticated,
    user,
    isSiteOwner,
    cannonical,
    title,
    headerType
  } = props;
  return (
    <React.Fragment>
      <Head>
        <title>{title}</title>
        <meta name="description" content="Maria Shmakova portfolio" />
        <meta
          name="keywords"
          content="shmakovs portfolio, shmakova developer, shmakova frontend"
        />
        <meta
          property="og:title"
          content="Maria Shmakova - frontend developer"
        />
        <meta property="og:local" content="ru_RU" />
        <meta property="og:url" content={process.env.BASE_URL} />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="Maria Shmakova portfolio" />
        <link
          href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap&subset=cyrillic"
          rel="stylesheet"
        />
        <link rel="icon" type="image/ico" href="/static/favicon.ico" />
        {cannonical && (
          <link
            rel="cannonical"
            href={`${process.env.BASE_URL}/${cannonical}`}
          />
        )}
      </Head>
      <div className="layout-container">
        <Header
          className={`port-nav-${headerType}`}
          isAuthenticated={isAuthenticated}
          user={user}
          isSiteOwner={isSiteOwner}
        />
        <main className={`${className}`}>
          <div className="wrapper">{children}</div>
        </main>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/ScrollToPlugin.min.js" />
    </React.Fragment>
  );
};

BaseLayout.defaultProps = {
  headerType: "default",
  title: "Maria Shmakova Portfolio",
  className: ""
};

export default BaseLayout;
