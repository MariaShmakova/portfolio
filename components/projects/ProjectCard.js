import React, { Component } from "react";
import { Card, CardHeader, CardBody, CardText } from "reactstrap";

class ProjectCard extends Component {
  renderSkills = skills => {
    if (!skills || skills.length === 0) return null;
    return (
      <ul className="project-skills">
        {skills.map((skill, index) => (
          <li className="project-skills__item" key={index}>{skill}</li>
        ))}
      </ul>
    );
  };

  render() {
    const { project } = this.props;

    return (
      <React.Fragment>
        <a target="_blank" href={project.link} className="project-link">
          <Card className="base-card project-card" onClick={this.handleToggle}>
            <CardHeader
              className="base-card-header"
              dangerouslySetInnerHTML={{
                __html: project.title
              }}
            />
            <CardBody>
              <div
                className="base-card-text"
                dangerouslySetInnerHTML={{
                  __html: project.description
                }}
              />
              <CardText>{this.renderSkills(project.skills)}</CardText>
            </CardBody>
          </Card>
        </a>
      </React.Fragment>
    );
  }
}

export default ProjectCard;
