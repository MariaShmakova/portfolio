import axios from "axios";
import { toast } from "react-toastify";
import logger from "./logService";

const instance = axios.create({
  baseURL: process.env.BASE_URL,
  timeout: 3000
});

instance.interceptors.response.use(null, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    toast.error("Unexpected error");
    logger.log(error);
  }

  return Promise.reject(error);
});

export default {
  get: instance.get,
  post: instance.post,
  put: instance.put,
  patch: instance.patch,
  delete: instance.delete
};
