import auth0 from "auth0-js";
import Cookies from "js-cookie";
import jwt from "jsonwebtoken";
import http from "./httpService";
import logger from "./logService";
import { getCookieFromReq } from "../helpers/utils";

class Auth0 {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_CLIENT_ID,
      redirectUri: `${process.env.BASE_URL}/callback`,
      responseType: "token id_token",
      scope: "openid profile"
    });

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
  }

  handleAuthentication() {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        console.error(err);
      }
    });
  }

  login() {
    this.auth0.authorize();
  }

  setSession(authResult) {
    Cookies.set("jwt", authResult.idToken);
  }

  logout() {
    Cookies.remove("jwt");

    this.auth0.logout({
      returnTo: process.env.BASE_URL,
      clientID: process.env.AUTH0_CLIENT_ID
    });
  }

  async getJWKS() {
    try {
      const response = await http.get(process.env.AUTH0_JWKS_URL);
      const jwks = response.data;
      return jwks;
    } catch (err) {
      console.error(err);
    }
  }

  async verifyToken(token) {
    if (!token) return;

    const jwks = await this.getJWKS();
    const jwk = jwks.keys[0];

    const decodedToken = jwt.decode(token, { complete: true });

    if (!decodedToken || jwk.kid !== decodedToken.header.kid) return;

    try {
      const cert = this.buildCertificate(jwk);
      const verifyToken = jwt.verify(token, cert);
      const expiresAt = verifyToken["exp"] * 1000;

      return verifyToken && new Date().getTime() < expiresAt
        ? verifyToken
        : undefined;
    } catch (err) {
      logger.error(err);
      console.error(err);
      return;
    }
  }

  buildCertificate(jwk) {
    let cert = jwk.x5c[0];
    cert = cert.match(/.{1,64}/g).join("\n");
    return `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`;
  }

  async clientAuth() {
    const token = Cookies.getJSON("jwt");
    return await this.verifyToken(token);
  }

  async serverAuth(req) {
    if (!req.headers.cookie) return;

    const token = getCookieFromReq(req, "jwt");

    return await this.verifyToken(token);
  }
}

const auth0Client = new Auth0();

export default auth0Client;
