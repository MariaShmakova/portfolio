import {
  init as sentryInit,
  captureMessage,
  captureException
} from "@sentry/browser";

function init() {
  sentryInit({
    dsn: "https://7dddbafd35ba434ab4caeda9899d4569@sentry.io/1478555"
  });
}

function log(message) {
  captureMessage(message);
}

function error(error) {
  captureException(error);
}

export default {
  init,
  log,
  error
};
