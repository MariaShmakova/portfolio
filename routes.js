const routes = require("next-routes");

module.exports = routes()
  .add({ page: "portfolios/create", pattern: "/portfolios/create" })
  .add({ page: "portfolios/edit", pattern: "/portfolios/:id/edit" })
  .add({ page: "blogs/create", pattern: "/blogs/create" })
  .add({ page: "blogs/dashboard", pattern: "/blogs/dashboard" })
  .add({ page: "blogs/detail", pattern: "/blogs/:slug" })
  .add({ page: "blogs/editorUpdate", pattern: "/blogs/:id/edit" });
