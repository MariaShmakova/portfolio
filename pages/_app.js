import React from "react";
import App, { Container } from "next/app";
import { ToastContainer } from "react-toastify";
import auth0 from "../services/auth0";
import logger from "../services/logService";
import Fonts from "../helpers/Fonts";

// stylings
import "bootstrap/dist/css/bootstrap.min.css";
import "react-toastify/dist/ReactToastify.css";
import "../styles/main.scss";

logger.init();

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    const user = process.browser
      ? await auth0.clientAuth()
      : await auth0.serverAuth(ctx.req);

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const isSiteOwner =
      user && user[process.env.NAMESPACE + "/role"] === "siteOwner";

    const auth = { user, isAuthenticated: !!user, isSiteOwner };
    
    return { pageProps, auth };
  }

  componentDidMount() {
    // Fonts();
  }

  componentDidCatch(error) {
    logger.error(error);
  }

  render() {
    const { Component, pageProps, auth } = this.props;

    return (
      <Container>
        <ToastContainer />
        <Component {...pageProps} auth={auth} />
      </Container>
    );
  }
}

export default MyApp;
