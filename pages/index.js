import React, { Component } from "react";
import BaseLayout from "../components/layout/BaseLayout";
import { Container, Col, Row } from "reactstrap";
import Typed from "react-typed";
import { Button } from "../components/slate-editor/components/index";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAnimated: true,
      roles: [
        "Frontend",
        "Vue.js",
        "React.js",
        "PHP",
        "Laravel",
        "Team Player"
      ],
      skills: [
        "JavaScript",
        "Vue",
        "Vuex",
        "React",
        "Redux",
        "Vuetify",
        "PHP",
        "Laravel",
        "SCSS"
      ]
    };
    this.scrollMagicController = null;
  }

  componentDidMount() {
    this.scrollMagicController = new ScrollMagic.Controller();
    this.animateGlitch();
    this.animateOpacity();
    this.animateFadein();
    this.animateScroll();
  }

  componentWillUnmount() {
    this.cardAnimationInterval && clearInterval(this.cardAnimationInterval);
  }

  animateGlitch = () => {
    this.cardAnimationInterval = setInterval(() => {
      this.setState({ isAnimated: !this.state.isAnimated });
    }, 10000);
  };

  animateOpacity = () => {
    new ScrollMagic.Scene({
      duration: 600
    })
      .setTween("#section-main", {
        opacity: 0
      })
      .addTo(this.scrollMagicController);
  };

  animateFadein = () => {
    new ScrollMagic.Scene({
      duration: 500,
      triggerElement: ".section-about"
    })
      .setTween(".section-about", {
        opacity: 1
      })
      .addTo(this.scrollMagicController);
  };

  smoothScroll = (scrollTo, h) => {
    let i = h || 0;
    if (i < scrollTo) {
      setTimeout(() => {
        window.scrollTo(0, i);
        this.smoothScroll(scrollTo, i + 10);
      }, 10);
    }
  };

  animateScroll = () => {
    new ScrollMagic.Scene({
      triggerElement: "#section-about",
      triggerHook: 1
    })
      .addTo(this.scrollMagicController)
      .on("enter", event => {
        this.smoothScroll(window.outerHeight);
      });
  };

  renderSkills = skills => {
    if (!skills || skills.length === 0) return null;
    return (
      <ul className="project-skills">
        {skills.map((skill, index) => (
          <li className="project-skills__item" key={index}>
            {skill}
          </li>
        ))}
      </ul>
    );
  };

  render() {
    const { user, isAuthenticated } = this.props.auth;
    const { isAnimated, skills } = this.state;

    return (
      <BaseLayout
        {...this.props.auth}
        className={`index-page`}
        headerType="index"
        title="Maria Shmakova - Portfolio"
      >
        <div id="section-main" className="section-main">
          <div className={`c-glitch ${isAnimated ? "animated" : ""}`}>
            <div className="c-glich__img" />
            <div className="c-glich__img" />
            <div className="c-glich__img" />
            <div className="c-glich__img" />
            <div className="c-glich__img" />
          </div>
          <Container className="welcome-block">
            <Row>
              <Col
                md={{ size: 6, offset: 6 }}
                className="welcome-block-wrapper"
              >
                <div className="welcome-block-text">
                  <h1>
                    {isAuthenticated && <b>{user.name}</b>} Welcome to the
                    portfolio website of Maria Shamkova. Get informed,
                    collaborate and discover projects I was working
                  </h1>
                </div>
                <Typed
                  loop
                  typeSpeed={60}
                  backSpeed={60}
                  strings={this.state.roles}
                  backDelay={1000}
                  showCursor
                  cursorChar="|"
                  className="self-typed"
                />
                <div className="welcome-block-bio">
                  <h2>Frontend Developer</h2>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="section-about" id="section-about">
          <Container>
            <Row>
              <Col md="6">
                <div className="left-side">
                  <h2 className="title fadein">Hello, Welcome</h2>
                  <p className="subsubTitle fadein only-not-xs">
                    Feel free to read short description about me.
                  </p>
                </div>
              </Col>
              <Col md="6">
                <div className="fadein">
                  <p>
                    My name is Maria Shmakova and I am an frontend developer.{" "}
                  </p>
                  <p className="only-not-xs">
                    I love to work on ambitious projects, collaborate with
                    passionate people, adopt emerging technologies and create
                    good stuff. I'm continuously trying to better myself and
                    still get challenged on a daily basis.
                  </p>
                  <p>
                    High ability to self-study, deep interest in the development
                    of sites allowed me to achieve considerable professionalism
                    in this direction.
                  </p>
                  <p>Knowledge and skills: </p>
                  {this.renderSkills(skills)}
                </div>
              </Col>
              <Col md="12">
                <a href="mailto:shmakova.work@gmail.com" className="btn">
                  Email
                </a>
                <a href="https://bitbucket.org/MariaShmakova" className="btn">
                  BitBucket
                </a>
                {/* <a href="https://vk.com/milka_durilka" className="btn">
                  VK
                </a> */}
              </Col>
            </Row>
          </Container>
        </div>
        {/* <svg style={{ display: "none" }}>
          <filter id="liquify">
            <feTurbulence
              baseFrequency="0.015"
              numOctaves="3"
              result="wrap"
              type="fractalNoise"
            />
            <feDisplacementMap
              id="liquid"
              in="SourceGraphic"
              in2="wrap"
              scale="250"
              xChannelSelector="R"
              yChannelSelector="B"
            />
          </filter>
        </svg> */}
      </BaseLayout>
    );
  }
}

export default Index;
