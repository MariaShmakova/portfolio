import React, { Component } from "react";
import BaseLayout from "../components/layout/BaseLayout";
import BasePage from "../components/BasePage";
import { Row, Col } from "reactstrap";

class Cv extends Component {
  state = {};
  render() {
    return (
      <BaseLayout {...this.props.auth} title="Maria Shmakova - CV">
        <BasePage title="Preview of my CV" className="cv-page">
          <Row>
            <Col md={{ size: 8, offset: 2 }}>
              <div className="cv-title">
                <a
                  download="shmakova_cv.pdf"
                  className="btn btn-success"
                  href="/static/cv.pdf"
                >
                  Download
                </a>
              </div>
              <iframe className="cv-iframe" src="/static/cv.pdf" />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default Cv;
