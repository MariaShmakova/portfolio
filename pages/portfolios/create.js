import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import PortfolioCreateForm from "../../components/porfolios/PortfolioCreateForm";
import { Row, Col } from "reactstrap";
import withAuth from "../../components/hoc/withAuth";
import { createPortfolio } from "../../actions";
import { toast } from "react-toastify";
import { Router } from "../../routes";

const INITIAL_VALUES = {
  title: "",
  company: "",
  location: "",
  position: "",
  description: "",
  startDate: "",
  endDate: ""
};

class PortfolioCreate extends Component {
  state = {};

  savePortfolio = (values, { setSubmitting }) => {
    setSubmitting(true);

    createPortfolio(values)
      .then(portfolio => {
        Router.pushRoute("/portfolios");
      })
      .catch(err => {
        toast.error(err.message);
      })
      .finally(() => setSubmitting(false));
  };

  render() {
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage
          className="portfolio-create-page"
          title="Portfolio Create Page"
        >
          <Row>
            <Col md="6">
              <PortfolioCreateForm
                initialValues={INITIAL_VALUES}
                onSubmit={this.savePortfolio}
              />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default withAuth("siteOwner")(PortfolioCreate);
