import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import PortfolioCreateForm from "../../components/porfolios/PortfolioCreateForm";
import { Row, Col } from "reactstrap";
import withAuth from "../../components/hoc/withAuth";
import { updatePortfolio, getPortfolioById } from "../../actions";
import { Router } from "../../routes";
import moment from "moment";

class PortfolioCreate extends Component {
  state = {};

  static async getInitialProps({ query }) {
    const portfolioId = query.id;
    let portfolio = {};

    try {
      portfolio = await getPortfolioById(portfolioId);
    } catch (err) {
      console.error(err);
    }

    return { portfolio };
  }

  updatePortfolio = (values, { setSubmitting }) => {
    setSubmitting(true);

    updatePortfolio(values)
      .then(portfolio => {
        Router.pushRoute("/portfolios");
      })
      .catch(err => {
        console.error(err.message);
      })
      .finally(() => setSubmitting(false));
  };

  render() {
    const { portfolio } = this.props;
    portfolio.startDate = moment(portfolio.startDate).toDate();
    portfolio.endDate = portfolio.endDate ? moment(portfolio.endDate).toDate() : null;

    return (
      <BaseLayout {...this.props.auth}>
        <BasePage className="portfolio-create-page" title="Portfolio Edit Page">
          <Row>
            <Col md="6">
              <PortfolioCreateForm
                initialValues={portfolio}
                onSubmit={this.updatePortfolio}
              />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default withAuth("siteOwner")(PortfolioCreate);
