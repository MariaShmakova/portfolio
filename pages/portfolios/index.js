import React, { Component } from "react";
import { Router } from "../../routes";
import { Row, Col, Button } from "reactstrap";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import { getPortfolios, deletePortfolio } from "../../actions";
import PortfolioCard from "../../components/porfolios/PortfolioCard";

class Portfolios extends Component {
  static async getInitialProps() {
    let portfolios = [];
    try {
      portfolios = await getPortfolios();
    } catch (err) {
      console.error(err);
    }

    return { portfolios };
  }

  navigateToEditPortfolio = (event, portfolioId) => {
    event.stopPropagation();
    Router.pushRoute(`/portfolios/${portfolioId}/edit`);
  };

  displayDeleteWarning = (event, portfolioId) => {
    event.stopPropagation();
    const isConfirm = confirm(
      "Are you sure you want to delete this portfolio???"
    );

    if (isConfirm) {
      this.deletePortfolio(portfolioId);
    }
  };
  deletePortfolio = async portfolioId => {
    try {
      const response = await deletePortfolio(portfolioId);
      Router.pushRoute("/portfolios");
    } catch (err) {
      console.error(err);
    }
  };

  renderPortfolios = portfolios => {
    const {
      auth: { isSiteOwner }
    } = this.props;

    return (
      portfolios.length > 0 &&
      portfolios.map((portfolio, index) => {
        return (
          <Col md="6" lg="4" key={portfolio._id}>
            <PortfolioCard portfolio={portfolio}>
              {isSiteOwner && (
                <React.Fragment>
                  <Button
                    color="warning"
                    onClick={event =>
                      this.navigateToEditPortfolio(event, portfolio._id)
                    }
                  >
                    Edit
                  </Button>{" "}
                  <Button
                    color="danger"
                    onClick={event => {
                      this.displayDeleteWarning(event, portfolio._id);
                    }}
                  >
                    Delete
                  </Button>
                </React.Fragment>
              )}
            </PortfolioCard>
          </Col>
        );
      })
    );
  };

  render() {
    const {
      portfolios,
      auth: { isSiteOwner }
    } = this.props;

    return (
      <BaseLayout
        {...this.props.auth}
        title="Maria Shmakova - Learn About My Career"
      >
        <BasePage className="portfolio-page" title="History Jobs">
          {isSiteOwner && (
            <Button
              color="success"
              className="create-port-btn"
              onClick={() => Router.pushRoute(`/portfolios/create`)}
            >
              Create new portfolio
            </Button>
          )}
          <Row>{this.renderPortfolios(portfolios)}</Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default Portfolios;
