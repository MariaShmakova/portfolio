import Document, { Html, Head, Main, NextScript } from "next/document";
import logger from "../services/logService";

process.on("unhandledRejection", err => {
  logger.error(err);
});

process.on("uncaughtException", err => {
  logger.error(err);
});

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
