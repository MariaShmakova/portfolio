import React, { Component } from "react";
import { Row, Col, Button } from "reactstrap";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import ProjectCard from "../../components/projects/ProjectCard";

class Projects extends Component {
  state = {
    projects: [
      {
        _id: 1,
        title: "Внешэкономбанк – годовой&nbsp;отчет",
        link: "http://annual2016.veb.ru",
        description:
          "Разработка адаптивной веб-версии годового отчета по готовым макетам",
        skills: ["Vue", "Sphinx"],
        images: []
      },
      {
        _id: 2,
        title: "Внешэкономбанк – нефинансовый&nbsp;отчет",
        link: "http://csr2016.veb.ru",
        description:
          "Разработка адаптивной веб-версии годового отчета по готовым макетам",
        skills: ["Vue", "Sphinx"],
        images: []
      },
      {
        _id: 3,
        title: "OpSpot – <br>интернет-магазин ",
        link: "https://opspot.ru",
        description: `Реализация небольших модулей: лайк, добавить в избранное, 
      поделиться (товаром, статьей, корзиной), модуль сопутствующих товаров, доработка модуля аналитики покупок`,
        skills: ["Laravel", "Vue", "PostgreSQL", "ChartJS"],
        images: []
      },
      {
        _id: 4,
        title: "Ачимгаз",
        link: "http://www.achimgaz.ru",
        description: `Участие в разработке административной части сайта`,
        skills: ["Laravel", "JS", "MySQL", "materialUI"],
        images: []
      },
      {
        _id: 5,
        title: "Две атмосферы",
        link: "https://2atm.ru",
        description: `Реализация адаптивной версии сайта`,
        skills: ["JS", "CSS3"],
        images: []
      },
      {
        _id: 6,
        title: "Simplecloud",
        link: "https://simplecloud.ru",
        description: `Поддержка фронтенда клиентской панели`,
        skills: ["Nuxt"],
        images: []
      },
      {
        _id: 7,
        title: "eLearning Server 5G",
        link: "",
        description: "Разработка нового интерфейса",
        skills: ["Vue", "Vuetify", "D3"]
      },
      {
        _id: 8,
        title: "eClass",
        link: "https://eclass.elearn.ru",
        description: "Модуль диагностики работы платформы. Поддержка проекта",
        skills: ["React", "Redux"]
      }
    ]
  };

  renderProjects = projects => {
    return (
      projects.length > 0 &&
      projects.map((project, index) => {
        return (
          <Col md="6" lg="4" key={project._id}>
            <ProjectCard project={project} />
          </Col>
        );
      })
    );
  };

  render() {
    const { projects } = this.state;

    return (
      <BaseLayout
        {...this.props.auth}
        title="Maria Shmakova - Learn About My Career"
      >
        <BasePage className="projects-page" title="Projects">
          <Row>{this.renderProjects(projects)}</Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default Projects;
