import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import { getBlogBySlug } from "../../actions";

class BlogDetail extends Component {
  state = {};
  static async getInitialProps({ query }) {
    const slug = query.slug;
    let blog = {};
    try {
      blog = await getBlogBySlug(slug);
    } catch (err) {
      console.error(err.message);
    }
    return { blog };
  }
  render() {
    const { blog } = this.props;
   
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage className="blog-detail-page">
          <Row>
            <Col md={{ size: 8, offset: 2 }}>
              <div dangerouslySetInnerHTML={{ __html: blog.story }} />
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default BlogDetail;
