import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import { Container, Row, Col, Button } from "reactstrap";
import withAuth from "../../components/hoc/withAuth";
import { Link, Router } from "../../routes";
import { toast } from "react-toastify";
import { getUserBlogs, updateBlog, deleteBlog } from "../../actions";
import PortButtomDropdown from "../../components/ButtonDropdown";

class BlogsDashboard extends Component {
  state = {};

  static async getInitialProps({ req }) {
    let userBlogs = [];
    try {
      userBlogs = await getUserBlogs(req);
    } catch (err) {
      console.error(err);
    }

    return { userBlogs };
  }

  separateBlogs(blogs) {
    let published = [];
    let drafts = [];

    blogs.forEach(blog => {
      if (blog.status === "draft") {
        drafts.push(blog);
      } else {
        published.push(blog);
      }
    });

    return { published, drafts };
  }

  changeStatus = (blogId, status) => {
    updateBlog({ _id: blogId, status })
      .then(response => Router.pushRoute("/userBlogs"))
      .catch(err => console.error(err));
  };

  deleteBlog = blogId => {
    deleteBlog(blogId)
      .then(() => {
        toast.success("Blog successfully deleted!");
        Router.pushRoute("/userBlogs");
      })
      .catch(err => {
        toast.error("Blog not deleted!");
        console.error(err.message);
      });
  };

  deleteBlogWarning = blogId => {
    const res = confirm("Are you sure you want to delete this blog post?");
    if (res) this.deleteBlog(blogId);
  };

  createStatus = status => {
    return status === "draft"
      ? { view: "Publish story", value: "published" }
      : { view: "Make a Draft", value: "draft" };
  };
  dropdownOptions = blog => {
    const blogStatus = this.createStatus(blog.status);

    return [
      {
        text: blogStatus.view,
        handlers: {
          onClick: () => this.changeStatus(blog._id, blogStatus.value)
        }
      },
      {
        text: "Delete",
        handlers: { onClick: () => this.deleteBlogWarning(blog._id) }
      }
    ];
  };

  renderBlogs = blogs => {
    return (
      <ul className="user-blogs-list">
        {blogs.map((blog, index) => (
          <li key={blog._id}>
            <Link route={`/blogs/${blog._id}/edit`}>
              <a>{blog.title}</a>
            </Link>
            <PortButtomDropdown items={this.dropdownOptions(blog)} />
          </li>
        ))}
      </ul>
    );
  };

  render() {
    const { userBlogs } = this.props;
    const { published, drafts } = this.separateBlogs(userBlogs);

    return (
      <BaseLayout
        headerType={"landing"}
        className="blog-user-page"
        {...this.props.auth}
      >
        <div
          className="masthead"
          style={{ backgroundImage: "url('/static/images/home-bg.jpg')" }}
        >
          <div className="overlay" />
          <Container>
            <div className="row">
              <div className="col-lg-8 col-md-10 mx-auto">
                <div className="site-heading">
                  <h1>Blogs Dashboard</h1>
                  <span className="subheading">
                    Let's write some nice blog today{" "}
                    <Link route="/blogs/create">
                      <Button color="primary">Create a new blog</Button>
                    </Link>
                  </span>
                </div>
              </div>
            </div>
          </Container>
        </div>
        <BasePage className="blog-body">
          <Row>
            <Col md="6" className="mx-auto text-center">
              <h2 className="blog-status-title">Published Blogs</h2>
              {this.renderBlogs(published)}
            </Col>
            <Col md="6" className="mx-auto text-center">
              <h2 className="blog-status-title">Draft Blogs</h2>
              {this.renderBlogs(drafts)}
            </Col>
          </Row>
        </BasePage>
      </BaseLayout>
    );
  }
}

export default withAuth("siteOwner")(BlogsDashboard);
