import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import withAuth from "../../components/hoc/withAuth";
import SlateEditor from "../../components/slate-editor/Editor";
import { createBlog } from "../../actions";
import { Router } from "../../routes";
import { toast } from "react-toastify";

class BlogCreate extends Component {
  state = {
    isSaving: false,
    lockId: Math.floor(1000 + Math.random() * 9000)
  };
  saveBlog = (story, heading) => {
    const { lockId } = this.state;
    this.setState({ isSaving: true });
    const blog = {};
    blog.title = heading.title;
    blog.subTitle = heading.subtitle;
    blog.story = story;

    createBlog(blog, lockId)
      .then(data => {
        toast.success("Blog successfully saved!");
        Router.pushRoute(`/blogs/${data._id}/edit`);
      })
      .catch(err => {
        const message = err.message || "Server error";
        toast.error(
          "Unexpected Error, Copy your progress and refresh browser."
        );
        console.error(message);
      })
      .finally(() => this.setState({ isSaving: false }));
  };

  render() {
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage containerClass="editor-wrapper" className="blog-editor-page">
          <SlateEditor isSaving={this.state.isSaving} onSave={this.saveBlog} />
        </BasePage>
      </BaseLayout>
    );
  }
}

export default withAuth("siteOwner")(BlogCreate);
