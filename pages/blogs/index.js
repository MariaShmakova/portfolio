import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import { Container, Row, Col } from "reactstrap";
import { Link } from "../../routes";
import moment from "moment";
import { getBlogs } from "../../actions";
import { shortenText } from "../../helpers/utils";

class Blogs extends Component {
  state = {};

  static async getInitialProps() {
    let blogs = [];

    try {
      blogs = await getBlogs();
    } catch (err) {
      console.error(err.message);
    }

    return { blogs };
  }

  renderPosts = blogs => {
    return blogs.map(blog => (
      <React.Fragment key={blog._id}>
        <div className="post-preview">
          <Link route={`/blogs/${blog.slug}`}>
            <a>
              <h2 className="post-title">{shortenText(blog.title)}</h2>
              <h3 className="post-subtitle">{shortenText(blog.subTitle)}</h3>
            </a>
          </Link>
          <p className="post-meta">
            Posted by <a href="#">{blog.author}</a>{" "}
            {moment(blog.createdAt).format("LLLL")}
          </p>
        </div>
        <hr />
      </React.Fragment>
    ));
  };

  render() {
    const { blogs } = this.props;

    return (
      <BaseLayout
        headerType={"landing"}
        className="blog-listing-page"
        {...this.props.auth}
        title="Maria Shmakova - Newest Blogs to read"
      >
        <div
          className="masthead"
          style={{ backgroundImage: "url('/static/images/home-bg.jpg')" }}
        >
          <div className="overlay" />
          <Container>
            <div className="row">
              <div className="col-lg-8 col-md-10 mx-auto">
                <div className="site-heading">
                  <h1>Blogs</h1>
                  <span className="subheading">Programming, travelling, notes...</span>
                </div>
              </div>
            </div>
          </Container>
        </div>
        <BasePage className="blog-body">
          <Row>
            <Col md="10" lg="8" className="mx-auto">
              {this.renderPosts(blogs)}
              <div className="clearfix">
                <a className="btn btn-primary float-right" href="#">
                  Older Posts &rarr;
                </a>
              </div>
            </Col>
          </Row>

          <footer>
            <Container>
              <Row>
                <div className="col-lg-8 col-md-10 mx-auto">
                  <ul className="list-inline text-center">
                    <li className="list-inline-item">
                      <a target="_blank" href="https://vk.com/milka_durilka">
                        <span className="fa-stack fa-lg">
                          <i className="fas fa-circle fa-stack-2x" />
                          <i className="fab fa-vk fa-stack-1x fa-inverse" />
                        </span>
                      </a>
                    </li>
                    <li className="list-inline-item">
                      <a
                        target="_blank"
                        href="https://bitbucket.org/MariaShmakova"
                      >
                        <span className="fa-stack fa-lg">
                          <i className="fas fa-circle fa-stack-2x" />
                          <i className="fab fa-bitbucket fa-stack-1x fa-inverse" />
                        </span>
                      </a>
                    </li>
                  </ul>
                  <p className="copyright text-muted">
                    Copyright &copy; Maria Shmakova 2019 
                  </p>
                </div>
              </Row>
            </Container>
          </footer>
        </BasePage>
        <style jsx>
          {`
            @import url("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css");
          `}
        </style>
      </BaseLayout>
    );
  }
}

export default Blogs;
