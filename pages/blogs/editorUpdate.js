import React, { Component } from "react";
import BaseLayout from "../../components/layout/BaseLayout";
import BasePage from "../../components/BasePage";
import withAuth from "../../components/hoc/withAuth";
import SlateEditor from "../../components/slate-editor/Editor";
import { getBlogById, updateBlog } from "../../actions";
import { toast } from "react-toastify";

class BlogEditor extends Component {
  state = {
    isSaving: false
  };

  static async getInitialProps({ query }) {
    const blogId = query.id;
    let blog = {};
    try {
      blog = await getBlogById(blogId);
    } catch (err) {
      console.error(err);
    }

    return { blog };
  }

  updateBlog = (story, heading) => {
    this.setState({ isSaving: true });
    const blog = {};
    blog.title = heading.title;
    blog.subTitle = heading.subtitle;
    blog.story = story;
    blog._id = this.props.blog._id;

    updateBlog(blog)
      .then(data => {
        toast.success("Blog successfully saved!");
      })
      .catch(err => {
        const message = err.response.data.message || "Server error";
        toast.error("Unexpected Error, Copy your progress and refresh browser.");
        console.error(message);
      })
      .finally(() => this.setState({ isSaving: false }));
  };

  render() {
    const { blog } = this.props;
    
    return (
      <BaseLayout {...this.props.auth}>
        <BasePage containerClass="editor-wrapper" className="blog-editor-page">
          <SlateEditor
            initialValue={blog.story}
            isSaving={this.state.isSaving}
            onSave={this.updateBlog}
          />
        </BasePage>
      </BaseLayout>
    );
  }
}

export default withAuth("siteOwner")(BlogEditor);
