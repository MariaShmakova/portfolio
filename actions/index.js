import http from "../services/httpService";
import Cookie from "js-cookie";
import { getCookieFromReq } from "../helpers/utils";

const setAuthHeader = req => {
  const token = req ? getCookieFromReq(req, "jwt") : Cookie.getJSON("jwt");
  if (!token) return;
  return { headers: { authorization: `Bearer ${token}` } };
};

export const getPortfolios = async () => {
  return await http.get("/api/v1/portfolios").then(response => response.data);
};

export const createPortfolio = async portfolioData => {
  return await http
    .post("/api/v1/portfolios", portfolioData, setAuthHeader())
    .then(response => response.data);
};

export const updatePortfolio = async portfolioData => {
  return await http
    .patch(
      `/api/v1/portfolios/${portfolioData._id}`,
      portfolioData,
      setAuthHeader()
    )
    .then(response => response.data);
};

export const getPortfolioById = async portfolioId => {
  return await http
    .get(`/api/v1/portfolios/${portfolioId}`)
    .then(response => response.data);
};

export const deletePortfolio = async portfolioId => {
  return await http
    .delete(`/api/v1/portfolios/${portfolioId}`, setAuthHeader())
    .then(response => response.data);
};

// -------------------- BLOG ACTIONS --------------

export const getUserBlogs = async req => {
  return await http
    .get("/api/v1/blogs/me", setAuthHeader(req))
    .then(response => response.data);
};

export const createBlog = (blogData, lockId) => {
  return http
    .post(`/api/v1/blogs?lockId=${lockId}`, blogData, setAuthHeader())
    .then(response => response.data);
};

export const getBlogById = blogId => {
  return http.get(`/api/v1/blogs/${blogId}`).then(response => response.data);
};

export const getBlogBySlug = slug => {
  return http.get(`/api/v1/blogs/s/${slug}`).then(response => response.data);
};

export const updateBlog = async blogData => {
  return await http
    .patch(`/api/v1/blogs/${blogData._id}`, blogData, setAuthHeader())
    .then(response => response.data);
};

export const deleteBlog = blogId => {
  return http
    .delete(`/api/v1/blogs/${blogId}`, setAuthHeader())
    .then(response => response.data);
};

export const getBlogs = () => {
  return http.get("/api/v1/blogs").then(response => response.data);
};
