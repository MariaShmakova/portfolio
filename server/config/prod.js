module.exports = {
  DB_URI: process.env.DB_URI,
  NAMESPACE: "https://shmakova-maria.herokuapp.com",
  AUTH0_CLIENT_ID: "UK5jYzRLQjkaa8n0XbfGfDNO8bCfODZD",
  AUTH0_DOMAIN: "dev-ar97jw4k.eu.auth0.com",
  AUTH0_JWKS_URL: "https://dev-ar97jw4k.eu.auth0.com/.well-known/jwks.json"
};
