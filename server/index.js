const express = require("express");
const next = require("next");
const routes = require("../routes");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const compression = require("compression");

//servieces
const authService = require("./middleware/auth");

//routes
const portfolioRoutes = require("./routes/portfolio");
const blogRoutes = require("./routes/blog");

const serverConfig = require("./config");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });

const handle = routes.getRequestHandler(app);

const robotsOptions = {
  root: path.join(__dirname, "../static"),
  headers: {
    "Content-type": "text/plain;charset=UTF-8"
  }
};

mongoose
  .connect(serverConfig.DB_URI, { useNewUrlParser: true })
  .then(() => console.log("Database connected!"))
  .catch(err => console.log("Database not connected!"));
app
  .prepare()
  .then(() => {
    const server = express();
    server.use(compression());
    server.use(bodyParser.json());

    server.use("/api/v1/portfolios", portfolioRoutes);
    server.use("/api/v1/blogs", blogRoutes);

    server.get("/robots.txt", (req, res) => {
      return res.status(200).sendFile("robots.txt", robotsOptions);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.use(function(err, req, res, next) {
      if (err.name === "UnauthorizedError") {
        res.status(401).send({
          title: "Unauthorized",
          detail: "Unauthorized Access!",
          error: err
        });
      }
    });

    const PORT = process.env.PORT || 3000;

    server.use(handle).listen(PORT, err => {
      if (err) throw err;
      console.log(`> Ready on port ${PORT}`);
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
