const express = require("express");
const router = express.Router();
const BlogController = require("../controllers/blog");
const auth = require("../middleware/auth");

router.get("", BlogController.getBlogs);
router.get(
  "/me",
  auth.checkJWT,
  auth.checkRole("siteOwner"),
  BlogController.getUserBlogs
);
router.get("/:id", BlogController.getBlogById);
router.get("/s/:slug", BlogController.getBlogBySlug);
router.post(
  "",
  auth.checkJWT,
  auth.checkRole("siteOwner"),
  BlogController.createBlog
);
router.patch(
  "/:id",
  auth.checkJWT,
  auth.checkRole("siteOwner"),
  BlogController.updatingBlog
);
router.delete(
  "/:id",
  auth.checkJWT,
  auth.checkRole("siteOwner"),
  BlogController.deleteBlog
);

module.exports = router;
