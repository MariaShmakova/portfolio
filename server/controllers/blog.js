const AsyncLock = require("async-lock");
const slugify = require("slugify");
const Blog = require("../models/blog");

const lock = new AsyncLock();

exports.getBlogs = (req, res) => {
  Blog.find({ status: "published" })
    .sort({ createdAt: -1 })
    .exec((err, foundBlogs) => {
      if (err) return res.status(422).send(err);

      return res.json(foundBlogs);
    });
};

exports.createBlog = (req, res) => {
  const lockId = req.query.lockId;

  if (lock.isBusy())
    return res.status(422).send({ message: "Blog is saving!!!", lock });

  lock.acquire(
    lockId,
    done => {
      const blogData = req.body;
      const blog = new Blog(blogData);

      if (req.user) {
        blog.userId = req.user.sub;
        blog.author = req.user.name;
      }

      blog.save((err, blogCreated) => {
        setTimeout(() => done(err), 5000);

        if (err) return res.status(422).send(err);

        return res.json(blogCreated);
      });
    },
    function(err, ret) {
      if (err) console.error(err);
    }
  );
};

exports.updatingBlog = (req, res) => {
  const blogId = req.params.id;
  const blogData = req.body;

  const blog = Blog.findById(blogId, (err, foundBlog) => {
    if (err) return res.status(422).send(err);

    if (blogData.status && blogData.status === "published" && !blogData.slug) {
      blogData.slug = slugify(foundBlog.title, {
        replacement: "-", // replace spaces with replacement
        remove: null, // regex to remove characters
        lower: true // result in lower case
      });
    }

    blogData.updatedAt = new Date();
    foundBlog.set(blogData);
    foundBlog.save((err, updatedBlog) => {
      if (err) return res.status(422).send(err);

      return res.json(updatedBlog);
    });
  });
};

exports.getBlogById = (req, res) => {
  const blogId = req.params.id;

  Blog.findById(blogId, (err, foundBlog) => {
    if (err) return res.status(422).send(err);

    return res.json(foundBlog);
  });
};

exports.getBlogBySlug = (req, res) => {
  const slug = req.params.slug;

  Blog.findOne({ slug }, (err, foundBlog) => {
    if (err) return res.status(422).send(err);

    return res.json(foundBlog);
  });
};

exports.getUserBlogs = (req, res) => {
  const userId = req.user.sub;

  Blog.find({ userId }, (err, foundBlogs) => {
    if (err) return res.status(422).send(err);

    return res.json(foundBlogs);
  });
};

exports.deleteBlog = (req, res) => {
  const blogId = req.params.id;

  Blog.deleteOne({ _id: blogId }, err => {
    if (err) return res.status(422).send(err);

    return res.json({ status: "DELETED" });
  });
};
