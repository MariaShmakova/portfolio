const Portfolio = require("../models/portfolio");

exports.getPorfolios = (req, res) => {
  Portfolio.find({})
    .sort({ startDate: -1 })
    .exec((err, allPorfolios) => {
      if (err) return res.status(422).send(err);

      return res.json(allPorfolios);
    });
};

exports.getPortfolioById = (req, res) => {
  const portfolioId = req.params.id;

  Portfolio.findById(portfolioId)
    .select("-__v")
    .exec((err, foundPorfolios) => {
      if (err) return res.status(422).send(err);

      return res.json(foundPorfolios);
    });
};

exports.savePortfolio = (req, res) => {
  const portfolioData = req.body;
  const userId = req.user && req.user.sub;

  const portfolio = new Portfolio(portfolioData);
  portfolio.userId = userId;

  portfolio.save((err, createdPortfolio) => {
    if (err) return res.status(422).send(err);

    return res.json(createdPortfolio);
  });
};

exports.updatePortfolio = (req, res) => {
  const portfolioId = req.params.id;
  const portfolioData = req.body;

  Portfolio.findById(portfolioId, (err, portfolio) => {
    if (err) return res.status(422).send(err);

    portfolio.set(portfolioData);
    portfolio.save((err, updatedPortfolio) => {
      if (err) return res.status(422).send(err);

      return res.json(updatedPortfolio);
    });
  });
};

exports.deletePortfolio = (req, res) => {
  const portfolioId = req.params.id;

  Portfolio.deleteOne({ _id: portfolioId }, err => {
    if (err) return res.status(422).send(err);

    return res.json({ status: "DELETED" });
  });
};
