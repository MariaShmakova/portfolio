const jwt = require("express-jwt");
const jwksRsa = require("jwks-rsa");
const config = require("../config");

const NAMESPACE = config.NAMESPACE;
const AUTH0_JWKS_URL = config.AUTH0_JWKS_URL;
const AUTH0_DOMAIN = config.AUTH0_DOMAIN;
const AUTH0_CLIENT_ID = config.AUTH0_CLIENT_ID;

exports.checkJWT = jwt({
  secret: jwksRsa.expressJwtSecret({
    jwksUri: AUTH0_JWKS_URL,
    rateLimit: true,
    cache: true,
    jwksRequestsPerMinute: 50
  }),

  audience: AUTH0_CLIENT_ID,
  issuer: `https://${AUTH0_DOMAIN}/`,
  algorithms: ["RS256"]
});

exports.checkRole = role => (req, res, next) => {
  const user = req.user;

  if (user && user[`${NAMESPACE}/role`] && user[`${NAMESPACE}/role`] === role) {
    next();
  } else {
    return res.status(401).send({
      title: "Not Authorized",
      detail: "You are not authorized to access this data"
    });
  }
};
